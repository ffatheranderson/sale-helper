package sale.helper;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class FileWriter {

    private final String pathToNewFile;
    private XSSFWorkbook wb = new XSSFWorkbook();

    public FileWriter(String originalFileName, String pathToDirectory) {
        this.pathToNewFile = pathToDirectory + File.separator + originalFileName
                .substring(0, originalFileName.lastIndexOf(".")) + "_PROCESSED.xlsx";
    }

    public void writeRowsIntoSheetWithName(String sheetNameToWriteInto, List<Row> rows) throws IOException {
        XSSFSheet sheet = wb.createSheet(sheetNameToWriteInto);
        AtomicInteger counter = new AtomicInteger(0);
        rows.forEach(r -> copyRowIntoANewSheet(sheet, r, counter));
    }

    public void createResultFileOrReplaceExistedOne() throws IOException {
        File f = new File(pathToNewFile);
        if (f.exists())
            if (!f.delete())
                throw new IOException(
                        "Can not remove file " + pathToNewFile + " please remove this file manually and try again.");

        Workbook wb = new HSSFWorkbook(App.class.getClassLoader().getResourceAsStream("result_template.xls"));
        try (FileOutputStream os = new FileOutputStream(pathToNewFile)) {
            wb.write(os);
        }
    }

    private void copyRowIntoANewSheet(Sheet s, Row original, AtomicInteger rowsCounter) {
        Row newRow = s.createRow(rowsCounter.getAndIncrement());

        original.forEach(c -> {
            Cell newCell = newRow.createCell(c.getColumnIndex(), c.getCellType());

            CellType cellType = c.getCellType();

            Optional.ofNullable(c.getHyperlink()).ifPresent(newCell::setHyperlink);
            newCell.setCellType(cellType);

            switch (cellType) {
                case BLANK:
                    break;
                case ERROR:
                    break;
                case STRING:
                    newCell.setCellValue(c.getRichStringCellValue());
                    break;
                case BOOLEAN:
                    newCell.setCellValue(c.getBooleanCellValue());
                    break;
                case FORMULA:
                    newCell.setCellValue(c.getCellFormula());
                    break;
                case NUMERIC:
                    newCell.setCellValue(c.getNumericCellValue());
                    break;
                default:
            }

        });
    }

    public void writeFile() throws IOException {
        try (FileOutputStream os = new FileOutputStream(pathToNewFile)) {
            wb.write(os);
        }
    }

    public String getPathToNewFile() {
        return pathToNewFile;
    }
}
