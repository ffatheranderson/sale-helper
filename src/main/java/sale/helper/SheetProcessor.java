package sale.helper;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.apache.commons.lang3.StringUtils.contains;

public class SheetProcessor {

    private static final SheetProcessor INSTANCE = new SheetProcessor();

    private SheetProcessor() {
    }

    public static SheetProcessor getInstance() {
        return INSTANCE;
    }

    public List<Row> getAllLeadsWithEmail(Sheet sheet) {
        return getRowsParalellStream(sheet)
                .filter(this::doesRowHaveEmail)
                .collect(Collectors.toList());
    }

    public List<Row> getTargetLeadsWithEmail(Sheet sheet, List<String> keyWords) {
        return getRowsParalellStream(sheet)
                .filter(this::doesRowHaveEmail)
                .filter(r -> doesRowHaveKeyWords(r, keyWords))
                .collect(Collectors.toList());
    }

    public List<Row> getNonTargetLeadsWithNoEmail(Sheet sheet, List<String> keyWords) {
        return getRowsParalellStream(sheet)
                .filter(r -> !doesRowHaveEmail(r))
                .filter(r -> !doesRowHaveKeyWords(r, keyWords))
                .collect(Collectors.toList());
    }

    public List<Row> getTargetLeadsWithNoEmail(Sheet sheet, List<String> keyWords) {
        return getRowsParalellStream(sheet)
                .filter(r -> !doesRowHaveEmail(r))
                .filter(r -> doesRowHaveKeyWords(r, keyWords))
                .collect(Collectors.toList());
    }

    private Stream<Row> getRowsParalellStream(Sheet sheet) {
        Iterable<Row> iterable = sheet::rowIterator;
        return StreamSupport.stream(iterable.spliterator(), true);
    }

    private boolean doesRowHaveEmail(Row row) {
        return getCellsParallelStream(row).filter(c -> CellType.STRING.equals(c.getCellType()))
                                          .anyMatch(c -> EmailValidator.getInstance().isValid(c.getStringCellValue()));
    }

    private boolean doesRowHaveKeyWords(Row row, List<String> keyWords) {
        return getCellsParallelStream(row).filter(c -> CellType.STRING.equals(c.getCellType()))
                                          .anyMatch(c -> stringContainsAnyOfKeyWords(c.getStringCellValue(), keyWords));
    }

    private boolean stringContainsAnyOfKeyWords(String s, List<String> keyWords) {
        return keyWords.stream().anyMatch(keyWord -> contains(s.toLowerCase(), keyWord));
    }

    private Stream<Cell> getCellsParallelStream(Row row) {
        Iterable<Cell> iterable = row::cellIterator;
        return StreamSupport.stream(iterable.spliterator(), true);
    }

}
