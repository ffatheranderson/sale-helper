package sale.helper.ui;

import org.apache.poi.ss.usermodel.Sheet;

public class SheetComboboxItem {
    final Sheet sheet;

    public SheetComboboxItem(Sheet sheet) {
        this.sheet = sheet;
    }

    @Override
    public String toString() {
        return sheet.getSheetName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SheetComboboxItem that = (SheetComboboxItem) o;

        return sheet.equals(that.sheet);
    }

    @Override
    public int hashCode() {
        return sheet.hashCode();
    }
}
